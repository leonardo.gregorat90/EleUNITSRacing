/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _COMMON_BASETYPES_H
#define _COMMON_BASETYPES_H

#include <stdint.h>
#include <stddef.h>

#define TRUE (1)
#define FALSE (0)

typedef char CHAR;
typedef char* PCHAR;

typedef unsigned char BYTE;
typedef unsigned char* PBYTE;
typedef unsigned short WORD;
typedef unsigned short* PWORD;
typedef unsigned int DWORD;
typedef unsigned int* PDWORD;
typedef unsigned long long QWORD;
typedef unsigned long long* PQWORD;

typedef signed char INT8;
typedef signed char* PINT8;
typedef short INT16;
typedef short* PINT16;
typedef int INT32;
typedef int* PINT32;
typedef long long INT64;
typedef long long* PINT64;

typedef float FLOAT;

typedef float* PFLOAT;

typedef BYTE BOOLEAN;

typedef void* PVOID;
typedef PVOID HANDLE;

typedef INT32 RESULT;

#if defined ( __CC_ARM )
  #define PERSISTENT __attribute__((section("persistent"),zero_init))
#elif defined ( __GNUC__ )
  #define PERSISTENT __attribute__((section(".persistent")))
#endif

#endif