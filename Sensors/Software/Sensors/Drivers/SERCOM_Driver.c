/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "samc21.h"
#include "SERCOM_Driver.h"
#include "SYSTEM_Driver.h"

static const Sercom* sercomPeriphPointers[] = {SERCOM0, SERCOM1, SERCOM2, SERCOM3, SERCOM4, SERCOM5};
static DWORD sercomInitStatus[6];
static HANDLE sercomIsrHandle[6];
static void (*sercomIsrHandler[6])(HANDLE);

static void sercom_clock_setup(Sercom *sercom);

void uart_isr_handler(HANDLE uphandle);

BOOLEAN UART_Initialize(UART_PERIPHERAL* periph, DWORD sercom_instance) {
    if(sercom_instance > 5)
        return FALSE;
    if(sercomInitStatus[sercom_instance] & 0x01)
        return FALSE;
    sercomInitStatus[sercom_instance] = 0x01;
    Sercom* sercom = sercomPeriphPointers[sercom_instance];
    periph->Sercom = sercom;
    periph->SercomInstance = sercom_instance;
    
    sercom_clock_setup(sercom);
    sercom->USART.CTRLA.reg = SERCOM_USART_CTRLA_SWRST;
	while (sercom->USART.SYNCBUSY.reg & SERCOM_USART_SYNCBUSY_SWRST);

    sercom->USART.CTRLA.reg = periph->ICtrlA;
	sercom->USART.CTRLB.reg = SERCOM_USART_CTRLB_RXEN | SERCOM_USART_CTRLB_TXEN | SERCOM_USART_CTRLB_CHSIZE(0x0);

    DWORD baud_t = SystemCoreClock / (periph->CurrentBaudRate * 2);
	DWORD br = (baud_t / 8) | ((baud_t % 8) << 13);
	sercom->USART.BAUD.reg = br;
	sercom->USART.CTRLA.bit.ENABLE = 1;

	sercom->USART.INTENSET.reg = SERCOM_USART_INTENSET_RXC;
    //Link ISR Hanlder;
    sercomIsrHandle[sercom_instance] = periph;
    sercomIsrHandler[sercom_instance] = uart_isr_handler;
    return TRUE;
}

BOOLEAN UART_LinkBuffers(UART_PERIPHERAL* periph, FIFO_BUFFER* rx_buffer, FIFO_BUFFER* tx_buffer) {
    periph->TxBuffer = tx_buffer;
    periph->RxBuffer = rx_buffer;
    FifoBuffer_SetLockFunc(tx_buffer, UART_TxLock, UART_TxUnlock, periph, FIFO_SYNCHRONIZE_WRITES);
    FifoBuffer_SetLockFunc(rx_buffer, UART_RxLock, UART_RxUnlock, periph, FIFO_SYNCHRONIZE_NO);
    return TRUE;
}

DWORD UART_TxLock(HANDLE periph) {
    UART_PERIPHERAL* uartp = (UART_PERIPHERAL*)periph;
    DWORD mask = uartp->Sercom->USART.INTENCLR.reg & SERCOM_USART_INTENCLR_DRE;
    uartp->Sercom->USART.INTENCLR.reg = SERCOM_USART_INTENCLR_DRE;
    return mask;
}

void UART_TxUnlock(HANDLE periph, DWORD lock) {
    UART_PERIPHERAL* uartp = (UART_PERIPHERAL*)periph;
    if(lock > 0)
        uartp->Sercom->USART.INTENSET.reg = SERCOM_USART_INTENSET_DRE;
}

DWORD UART_RxLock(HANDLE periph) {
    UART_PERIPHERAL* uartp = (UART_PERIPHERAL*)periph;
    DWORD mask = uartp->Sercom->USART.INTENCLR.reg & SERCOM_USART_INTENCLR_RXC;
    uartp->Sercom->USART.INTENCLR.reg = SERCOM_USART_INTENCLR_RXC;
    return mask;
}

void UART_RxUnlock(HANDLE periph, DWORD lock) {
    UART_PERIPHERAL* uartp = (UART_PERIPHERAL*)periph;
    if(lock > 0)
        uartp->Sercom->USART.INTENSET.reg = SERCOM_USART_INTENSET_RXC;
}

BOOLEAN UART_SetMode(UART_PERIPHERAL* periph, DWORD baudrate, DWORD mode) {
    if(baudrate > 3000000)
        return FALSE;
    periph->CurrentBaudRate = baudrate;
    if(mode == UART_MODE_NORMAL) {
        periph->ICtrlA = SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_RXPO(0x1) | SERCOM_USART_CTRLA_TXPO(0x0) | SERCOM_USART_CTRLA_MODE(0x1) | SERCOM_USART_CTRLA_SAMPR(0x01);
    } else if(mode == UART_MODE_RS485){
        periph->ICtrlA = SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_RXPO(0x1) | SERCOM_USART_CTRLA_TXPO(0x3) | SERCOM_USART_CTRLA_MODE(0x1) | SERCOM_USART_CTRLA_SAMPR(0x01);
    } else if(mode == UART_MODE_FLOWCONTROL){
        periph->ICtrlA = SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_RXPO(0x1) | SERCOM_USART_CTRLA_TXPO(0x2) | SERCOM_USART_CTRLA_MODE(0x1) | SERCOM_USART_CTRLA_SAMPR(0x01);
    }
    return TRUE;
}

void uart_isr_handler(HANDLE uphandle)
{
    UART_PERIPHERAL* periph = (UART_PERIPHERAL*)uphandle;
    BYTE intsts =  periph->Sercom->USART.INTFLAG.reg;
	if(intsts & SERCOM_USART_INTFLAG_RXC) {
		CHAR data = periph->Sercom->USART.DATA.reg;
		FifoBuffer_WriteByteFromISR(periph->RxBuffer, data);
	}

	if(intsts & SERCOM_USART_INTFLAG_DRE) {
		CHAR data;
		if(!FifoBuffer_ReadByteFromISR(periph->TxBuffer, &data))
		{
			periph->Sercom->USART.INTENCLR.reg = SERCOM_USART_INTENCLR_DRE;
			return;
		}
		periph->Sercom->USART.DATA.reg = data;
	}
}

BOOLEAN I2C_Initialize(I2C_PERIPHERAL* periph, DWORD sercom_instance) {
    if(sercom_instance > 5)
        return FALSE;
    if(sercomInitStatus[sercom_instance] & 0x01)
        return FALSE;
    sercomInitStatus[sercom_instance] = 0x03;
    Sercom* sercom = sercomPeriphPointers[sercom_instance];
    periph->Sercom = sercom;
    periph->SercomInstance = sercom_instance;
    
    sercom_clock_setup(sercom);
    sercom->USART.CTRLA.reg = SERCOM_USART_CTRLA_SWRST;
	while (sercom->I2CM.SYNCBUSY.reg & SERCOM_USART_SYNCBUSY_SWRST);

    return TRUE;
}

static void sercom_clock_setup(Sercom *sercom) {
    if(sercom == SERCOM0) {
        MCLK->APBCMASK.reg |= MCLK_APBCMASK_SERCOM0;
	    GCLK->PCHCTRL[SERCOM0_GCLK_ID_CORE].reg = GCLK_PCHCTRL_GEN(0) | GCLK_PCHCTRL_CHEN;
	    while (0 == (GCLK->PCHCTRL[SERCOM0_GCLK_ID_CORE].reg & GCLK_PCHCTRL_CHEN));
    } else if(sercom == SERCOM1) {
        MCLK->APBCMASK.reg |= MCLK_APBCMASK_SERCOM1;
	    GCLK->PCHCTRL[SERCOM1_GCLK_ID_CORE].reg = GCLK_PCHCTRL_GEN(0) | GCLK_PCHCTRL_CHEN;
	    while (0 == (GCLK->PCHCTRL[SERCOM1_GCLK_ID_CORE].reg & GCLK_PCHCTRL_CHEN));
    } else if(sercom == SERCOM2) {
        MCLK->APBCMASK.reg |= MCLK_APBCMASK_SERCOM2;
	    GCLK->PCHCTRL[SERCOM2_GCLK_ID_CORE].reg = GCLK_PCHCTRL_GEN(0) | GCLK_PCHCTRL_CHEN;
	    while (0 == (GCLK->PCHCTRL[SERCOM2_GCLK_ID_CORE].reg & GCLK_PCHCTRL_CHEN));
    } else if(sercom == SERCOM3) {
        MCLK->APBCMASK.reg |= MCLK_APBCMASK_SERCOM3;
	    GCLK->PCHCTRL[SERCOM3_GCLK_ID_CORE].reg = GCLK_PCHCTRL_GEN(0) | GCLK_PCHCTRL_CHEN;
	    while (0 == (GCLK->PCHCTRL[SERCOM3_GCLK_ID_CORE].reg & GCLK_PCHCTRL_CHEN));
    } else if(sercom == SERCOM4) {
        MCLK->APBCMASK.reg |= MCLK_APBCMASK_SERCOM4;
	    GCLK->PCHCTRL[SERCOM4_GCLK_ID_CORE].reg = GCLK_PCHCTRL_GEN(0) | GCLK_PCHCTRL_CHEN;
	    while (0 == (GCLK->PCHCTRL[SERCOM4_GCLK_ID_CORE].reg & GCLK_PCHCTRL_CHEN));
    } else if(sercom == SERCOM5) {
        MCLK->APBCMASK.reg |= MCLK_APBCMASK_SERCOM5;
	    GCLK->PCHCTRL[SERCOM5_GCLK_ID_CORE].reg = GCLK_PCHCTRL_GEN(0) | GCLK_PCHCTRL_CHEN;
	    while (0 == (GCLK->PCHCTRL[SERCOM5_GCLK_ID_CORE].reg & GCLK_PCHCTRL_CHEN));
    }
    if(SYSTEM_IsClockSourceAvailable(SYSTEM_CLOCK_SOURCE_32kHZULP_GCLK7)) {
        GCLK->PCHCTRL[SERCOM0_GCLK_ID_SLOW].reg = GCLK_PCHCTRL_GEN(7) | GCLK_PCHCTRL_CHEN;
    }
}

void SERCOM0_Handler() {
    sercomIsrHandler[0](sercomIsrHandle[0]);
}

void SERCOM1_Handler() {
    sercomIsrHandler[1](sercomIsrHandle[1]);
}

void SERCOM2_Handler() {
    sercomIsrHandler[2](sercomIsrHandle[2]);
}

void SERCOM3_Handler() {
    sercomIsrHandler[3](sercomIsrHandle[3]);
}

void SERCOM4_Handler() {
    sercomIsrHandler[4](sercomIsrHandle[4]);
}

void SERCOM5_Handler() {
    sercomIsrHandler[5](sercomIsrHandle[5]);
}