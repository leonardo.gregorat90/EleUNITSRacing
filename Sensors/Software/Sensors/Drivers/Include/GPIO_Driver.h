/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _GPIO_DRIVER_H
#define _GPIO_DRIVER_H

#include "common.h"
#include "samc21.h"

#define GPIO_PINMODE_INPUT         0
#define GPIO_PINMODE_INPUT_PULLUP  1
#define GPIO_PINMODE_OUTPUT        2
#define GPIO_PINMODE_ANALOG        3

#define GPIO_PINMUX_EXTINT         0
#define GPIO_PINMUX_ANALOG         1
#define GPIO_PINMUX_SERCOM         2
#define GPIO_PINMUX_SERCOMALT      3
#define GPIO_PINMUX_TC             4
#define GPIO_PINMUX_TCC            5
#define GPIO_PINMUX_CANSWD         6
#define GPIO_PINMUX_ACCLKS         7
#define GPIO_PINMUX_CCL            8
#define GPIO_PINMUX_DISABLE        10

#define GPIO_PORTA                 0
#define GPIO_PORTB                 1

void GPIO_SetPinMode(DWORD port, DWORD pin, DWORD mode);
void GPIO_SetPinMux(DWORD port, DWORD pin, DWORD function);
BOOLEAN GPIO_GetPinState(DWORD port, DWORD pin);

#define GPIO_FastPinModeInput(port, pin)    (PORT->Group[(port)].DIRCLR.reg = 1 << (pin))
#define GPIO_FastPinModeOutput(port, pin)    (PORT->Group[(port)].DIRSET.reg = 1 << (pin))
#define GPIO_SetPinHigh(port, pin)    (PORT->Group[port].OUTSET.reg = 1 << (pin))
#define GPIO_SetPinLow(port, pin)    (PORT->Group[port].OUTCLR.reg = 1 << (pin))

#endif
