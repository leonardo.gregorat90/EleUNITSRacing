/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "samc21.h"
#include "MCAN_Driver.h"
#include "MCAN_Config.h"
#include "MCAN_Internal.h"

#if(MCAN0_ENABLED)

DWORD _MCAN0_message_ram[MCAN0_MESSAGE_RAM_SIZE];

MCAN_PERIPHERAL MCAN0 = {
	.Peripheral = CAN0,
	.Config = &MCAN0_Config,
	.Memory = { .pRamStandardFilters = _MCAN0_message_ram }
};

#endif

#if(MCAN1_ENABLED)

DWORD _MCAN1_message_ram[MCAN0_MESSAGE_RAM_SIZE];

MCAN_PERIPHERAL MCAN1 = {
	.Peripheral = CAN1,
	.Config = &MCAN1_Config,
	.Memory = { .pRamStandardFilters = _MCAN1_message_ram }
};

#endif

void MCAN_Initialize(MCAN_PERIPHERAL* instance)
{
	DWORD* message_ram = instance->Memory.pRamStandardFilters;
	message_ram += instance->Config->RxStandardFilters;
	instance->Memory.pRamExtendedFilters = message_ram;
	message_ram += instance->Config->RxExtendedFilters * WORDS_PER_EXTENDED_FILTER;
	instance->Memory.pRamRXFifo0 = message_ram;
	message_ram += instance->Config->RxFifo0Length * WORDS_PER_FIFO_ELEMENT;
	instance->Memory.pRamRXFifo1 = message_ram;
	message_ram += instance->Config->RxFifo1Length * WORDS_PER_FIFO_ELEMENT;
	instance->Memory.pRamRXDedicated = message_ram;
	message_ram += instance->Config->RxDedicatedBuffers * WORDS_PER_FIFO_ELEMENT;
	instance->Memory.pRamTXEvent = message_ram;
	message_ram += instance->Config->TxEventFifoLenght * WORDS_PER_FIFO_ELEMENT;
	instance->Memory.pRamTXBuffers = message_ram;

	if(instance->Peripheral == CAN0){
		MCLK->AHBMASK.reg |= MCLK_AHBMASK_CAN0;
		GCLK->PCHCTRL[CAN0_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN_GCLK0;
		while(GCLK->PCHCTRL[CAN0_GCLK_ID].bit.CHEN == 0){} 
	} else if(instance->Peripheral == CAN1) {
		MCLK->AHBMASK.reg |= MCLK_AHBMASK_CAN1;
		GCLK->PCHCTRL[CAN1_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN_GCLK0;
		while(GCLK->PCHCTRL[CAN1_GCLK_ID].bit.CHEN == 0){} 
	}
	//Enter configuration mode
	instance->Peripheral->CCCR.reg = CAN_CCCR_INIT;
	while((instance->Peripheral->CCCR.reg & CAN_CCCR_INIT) == 0){}
	instance->Peripheral->CCCR.reg |= CAN_CCCR_CCE;
	instance->CurrentMode = MCAN_MODE_CONFIGURE;
	//Load Message RAM Addresses
	instance->Peripheral->SIDFC.reg = CAN_SIDFC_FLSSA((DWORD)instance->Memory.pRamStandardFilters) | CAN_SIDFC_LSS(instance->Config->RxStandardFilters);
	instance->Peripheral->XIDFC.reg = CAN_XIDFC_FLESA((DWORD)instance->Memory.pRamExtendedFilters) | CAN_XIDFC_LSE(instance->Config->RxExtendedFilters);
	instance->Peripheral->RXBC.reg = CAN_RXBC_RBSA((DWORD)instance->Memory.pRamRXDedicated);
	instance->Peripheral->RXF0C.reg = CAN_RXF0C_F0SA((DWORD)instance->Memory.pRamRXFifo0) | CAN_RXF0C_F0S(instance->Config->RxFifo0Length);
	instance->Peripheral->RXF1C.reg = CAN_RXF1C_F1SA((DWORD)instance->Memory.pRamRXFifo1) | CAN_RXF1C_F1S(instance->Config->RxFifo1Length);
	instance->Peripheral->TXBC.reg = CAN_TXBC_TBSA((DWORD)instance->Memory.pRamTXBuffers) | CAN_TXBC_NDTB(instance->Config->TxDedicatedBuffers) | CAN_TXBC_TFQS(instance->Config->TxFifoLenght);
	instance->Peripheral->TXEFC.reg = CAN_TXEFC_EFSA((DWORD)instance->Memory.pRamTXEvent) | CAN_TXEFC_EFS(instance->Config->TxEventFifoLenght);
	//Configure Bit Timings (may be changed at runtime after initialization)
	instance->Peripheral->NBTP.reg = instance->Config->BitTimings;
	instance->Peripheral->GFC.reg = instance->Config->GlobalFilter;
	instance->Peripheral->XIDAM.reg = instance->Config->ExtendedIdAndMask;
	//Preload inital filters
	if(instance->Config->StdInitialFiltersNum && instance->Config->StdInitialFilters) {
		DWORD* pstdfilt = instance->Memory.pRamStandardFilters;
		for(DWORD i = 0; i < instance->Config->StdInitialFiltersNum; i++){
			*pstdfilt++ = instance->Config->StdInitialFilters[i];
		}
	}
	if(instance->Config->ExtInitialFiltersNum && instance->Config->ExtInitialFilters) {
		DWORD* pextfilt = instance->Memory.pRamExtendedFilters;
		for(DWORD i = 0; i < instance->Config->ExtInitialFiltersNum * WORDS_PER_EXTENDED_FILTER; i++){
			*pextfilt++ = instance->Config->ExtInitialFilters[i];
		} 
	}
	//CAN0->CCCR.reg |= CAN_CCCR_PXHD;
	/*NVIC_EnableIRQ(CAN0_IRQn);
	CAN0->ILE.bit.EINT0 = 1;
	CAN0->IE.bit.RF0NE = 1;*/
}
 
BOOLEAN MCAN_ConfigureBitTimings(MCAN_PERIPHERAL *instance, DWORD sjw, DWORD seg1, DWORD seg2, DWORD prescaler)
{
	if(instance->CurrentMode != MCAN_MODE_CONFIGURE)
		return FALSE;
	if(sjw < 1 || prescaler < 1 || seg1 < 2 || seg2 < 1)
		return FALSE;
	if(sjw >= 128 || prescaler >= 512 || seg1 >= 256 || seg2 >= 128)
		return FALSE;
	instance->Peripheral->NBTP.reg = CAN_NBTP_NSJW(sjw - 1) | CAN_NBTP_NBRP(prescaler - 1) | CAN_NBTP_NTSEG1(seg1 - 1) | CAN_NBTP_NTSEG2(seg2 - 1);
	return TRUE;
}

BOOLEAN MCAN_ConfigureGlobalFilter(MCAN_PERIPHERAL *instance, DWORD remote_frame, DWORD non_matching_std, DWORD non_matching_ext)
 {
	if(instance->CurrentMode != MCAN_MODE_CONFIGURE)
		return FALSE;
	if(non_matching_ext > 3 || non_matching_std > 2 || non_matching_ext > 2)
		return FALSE;
	instance->Peripheral->GFC.reg = remote_frame | CAN_GFC_ANFE(non_matching_ext) | CAN_GFC_ANFS(non_matching_std);
	return TRUE;
}

BOOLEAN MCAN_ConfigureExtendedIdGlobalMask(MCAN_PERIPHERAL *instance, DWORD ext_id_mask)
{
	if(instance->CurrentMode != MCAN_MODE_CONFIGURE)
		return FALSE;
	instance->Peripheral->XIDAM.reg = ext_id_mask;
	return TRUE;
}

BOOLEAN MCAN_LoadStandardFilter(MCAN_PERIPHERAL *instance, DWORD filternum, DWORD filter) {
	if(filternum >= instance->Config->RxStandardFilters)
		return FALSE;
	DWORD* pstdfilt = instance->Memory.pRamStandardFilters + filternum;
	*pstdfilt = filter;
	return TRUE;
}

BOOLEAN MCAN_LoadExtendedFilter(MCAN_PERIPHERAL *instance, DWORD filternum, DWORD* filter) {
	if(filternum >= instance->Config->RxExtendedFilters)
		return FALSE;
	DWORD* pextfilt = instance->Memory.pRamExtendedFilters + filternum * WORDS_PER_EXTENDED_FILTER;
	*pextfilt++ = *filter++;
	*pextfilt = *filter;
	return TRUE;
}

BOOLEAN MCAN_ChangeMode(MCAN_PERIPHERAL *instance, DWORD mode)
{
	if(instance->CurrentMode == MCAN_MODE_CONFIGURE || instance->CurrentMode == MCAN_MODE_DISABLED) {
		if(mode == MCAN_MODE_ENABLED || mode == MCAN_MODE_MONITOR){
			instance->Peripheral->CCCR.bit.MON = (mode == MCAN_MODE_MONITOR) ? 1 : 0;
			instance->Peripheral->CCCR.bit.INIT = 0; //Activate the CAN Peripheral
			instance->CurrentMode = mode;
			return TRUE;
		}else if(mode == MCAN_MODE_CONFIGURE) {
			instance->Peripheral->CCCR.reg |= CAN_CCCR_CCE;
			instance->CurrentMode = MCAN_MODE_CONFIGURE;
			return TRUE;
		}
	}else if(instance->CurrentMode == MCAN_MODE_ENABLED || instance->CurrentMode == MCAN_MODE_MONITOR) {
		if(mode == MCAN_MODE_DISABLED || mode == MCAN_MODE_CONFIGURE) {
			instance->Peripheral->CCCR.bit.INIT = 0; //Activate the CAN Peripheral
			while((instance->Peripheral->CCCR.reg & CAN_CCCR_INIT) == 0){}
			if(mode == MCAN_MODE_CONFIGURE)
				instance->Peripheral->CCCR.reg |= CAN_CCCR_CCE;
			instance->CurrentMode = mode;
					return TRUE;
		}
	}
	return FALSE;
}

DWORD MCAN_GetCurrentMode(MCAN_PERIPHERAL *instance)
{
	return instance->CurrentMode;
}

BOOLEAN MCAN_ReadFIFOMessage(MCAN_PERIPHERAL *instance, DWORD fifo, MCAN_MESSAGE *msg)
{
	BYTE get_index, fill_level;
	DWORD* bufElement;
	if(fifo == 0) {
		get_index = instance->Peripheral->RXF0S.bit.F0GI;
		fill_level= instance->Peripheral->RXF0S.bit.F0FL;
		bufElement = instance->Memory.pRamRXFifo0 + get_index * WORDS_PER_FIFO_ELEMENT;
	} else if(fifo == 1) {
		get_index = instance->Peripheral->RXF1S.bit.F1GI;
		fill_level= instance->Peripheral->RXF1S.bit.F1FL;
		bufElement = instance->Memory.pRamRXFifo1 + get_index * WORDS_PER_FIFO_ELEMENT;
	} else {
		return FALSE;
	}
	if (fill_level == 0)
		return FALSE;
	if(*bufElement & MCAN_RAM_BUF_XTD) {
		msg->Id = *bufElement & MCAN_RAM_BUF_ID_XTD_Msk;
		msg->Options = MCAN_MESSAGE_OPTION_EXTENDEDID;
	} else {
		msg->Id = (*bufElement & MCAN_RAM_BUF_ID_STD_Msk) >> MCAN_RAM_BUF_ID_STD_Pos;
	}

	if(*bufElement & MCAN_RAM_BUF_RTR) {
		msg->Options |= MCAN_MESSAGE_OPTION_REMOTEFRAME;
		msg->Lenght = 0;
	} else {
		bufElement +=1;
		BYTE len = (*bufElement & MCAN_RAM_BUF_DLC_Msk) >> MCAN_RAM_BUF_DLC_Pos;
		if(len > 8) len = 8;
		msg->Lenght = len;
		if(len > 0) {
			bufElement +=1;
			//Copy two data DWORDs
			DWORD* dataPtr = (DWORD*)msg->Data;
			*dataPtr++ = *bufElement++;
			*dataPtr = *bufElement;
		}
	}
	if(fifo == 0)
		instance->Peripheral->RXF0A.reg = get_index;
	else
		instance->Peripheral->RXF1A.reg = get_index;
	return TRUE;
}

BOOLEAN MCAN_TransmitMessageFIFO(MCAN_PERIPHERAL *instance, MCAN_MESSAGE *msg)
{
	if(instance->Peripheral->TXFQS.bit.TFQF == 1)
		return FALSE;
	BYTE put_index = instance->Peripheral->TXFQS.bit.TFQPI;
	DWORD* bufElement = instance->Memory.pRamTXBuffers + put_index * WORDS_PER_FIFO_ELEMENT;
	if(msg->Options & MCAN_MESSAGE_OPTION_EXTENDEDID)
		*bufElement++ = MCAN_RAM_BUF_ID_XTD(msg->Id) | MCAN_RAM_BUF_ID_XTD(1);
	else
		*bufElement++ = MCAN_RAM_BUF_ID_STD(msg->Id);
	*bufElement++ = MCAN_RAM_BUF_DLC(msg->Lenght);
	//Copy two data DWORDs
	DWORD* dataPtr = (DWORD*)msg->Data;
	*bufElement++ = *dataPtr++;
	*bufElement = *dataPtr;
	instance->Peripheral->TXBAR.reg |= (1 << put_index);
	return TRUE;
}