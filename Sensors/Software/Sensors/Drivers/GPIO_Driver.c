/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "GPIO_Driver.h"

void GPIO_SetPinMode(DWORD port, DWORD pin, DWORD mode)
{
    if(mode == GPIO_PINMODE_INPUT) {
        PORT->Group[port].DIRCLR.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = PORT_PINCFG_INEN;
    } else if (mode == GPIO_PINMODE_INPUT_PULLUP) {
        PORT->Group[port].DIRCLR.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = PORT_PINCFG_INEN | PORT_PINCFG_PULLEN;
    } else if (mode == GPIO_PINMODE_OUTPUT) {
        PORT->Group[port].DIRSET.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = PORT_PINCFG_INEN;
    } else if (mode == GPIO_PINMODE_ANALOG) {
        PORT->Group[port].DIRCLR.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = 0;
    }
}

void GPIO_SetPinMux(DWORD port, DWORD pin, DWORD function){
    PORT->Group[port].PINCFG[pin].bit.PMUXEN = 0;
    if(function >= GPIO_PINMUX_DISABLE)
        return;
    PORT->Group[port].PMUX[pin / 2].reg &= ~(0x0F << ((pin % 2) * 4));
    PORT->Group[port].PMUX[pin / 2].reg |= (function & 0x0F) << ((pin % 2) * 4);
    PORT->Group[port].PINCFG[pin].bit.PMUXEN = 1;
}

//Funzioni candidate per la trasformazione in macro

BOOLEAN GPIO_GetPinState(DWORD port, DWORD pin) {
    if(PORT->Group[port].IN.reg & (1 << pin))
        return TRUE;
    else
        return FALSE;
}

