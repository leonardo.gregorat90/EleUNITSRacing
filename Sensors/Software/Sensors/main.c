#include "SAMC21_BSP.h"

//PROVA PUSH

//ONE AND ONLY ONE OF THE FOLLOWING HAS TO BE HIGH ACCORDING TO WHAT YOU ARE USING
#define FRONTSENSORS 0 //Specify if the config is for the front sensors
#define REARSENSORS 1 //Specify if the config is for the rear sensors

//Macro to use only for the register EVCTRL, INTENCLR, INTENSET, INTENFLAG, ASYNCH, DEBOUNCEN and PINSTATE of the EIC
#define EXTINT(value)	(1 << value)

#define PWR_FAULT_TRIES 5


#define SUSPENSION_freq 100 //Suspensions sampling rate
#define ENGINETEMP_freq 5 //Engine temperature sampling rate
#define SUSPENSION_T (1000/SUSPENSION_freq) //Suspensions period in ms
#define ENGINETEMP_T (1000/ENGINETEMP_freq) //Engine temperature period in ms

#if(REARSENSORS)
#define PRESSURE_freq 5 //Water pressure sampling rate
#define WATERTEMP_freq 2 //Water temperature sampling rate
#define PRESSURE_T (1000/PRESSURE_freq) //Water pressure period in ms
#define WATERTEMP_T (1000/WATERTEMP_freq) //Water temperature period in ms

#define CANID_ONLINE 0x80 //Board online
#define CANID_SUSPENSION 0x8A //Suspension position (left,right)
#define CANID_ENGINETEMP 0x8B //Engine temperature (left,right)
#define CANID_PRESSURE 0x8C //Water pressure
#define CANID_WATERTEMP0 0x8D //Water temperature 0-3 sensors (0, 1, 2, 3)
#define CANID_TEMPERATURE1 0x8E //Water temperature 4-7 sensors (4, 5, 6, 7)
#endif

#if(FRONTSENSORS)
#define BRAKETEMP_freq 10 //Brake oil temperature sampling rate
#define LOADCELL_freq 40 //Steering and brake pedal load cells sampling rate
#define BRAKETEMP_T (1000/BRAKETEMP_freq) //Brake oil temperature period in ms
#define LOADCELL_T (1000/LOADCELL_freq) /Steering and brake pedal load cells period in ms

#define CANID_ONLINE 0xA0 //Board online
#define CANID_SUSPENSION 0xAA //Suspension position (left,right)
#define CANID_ENGINETEMP 0xAB //Engine temperature (left,right)
#define CANID_BRAKETEMP 0xAC //Brake temperature (left,right)
#define CANID_LOADCELL 0xAD //Load cells (stearing0, stearing1, brake)
#endif

// UART and CAN variables
CHAR strbuf[80];
UART_PERIPHERAL UART0_PERIPH;
FIFO_BUFFER UART0_RXBUFFER, UART0_TXBUFFER;
CHAR _tx_fifo_mem[96], _rx_fifo_mem[96];
MCAN_MESSAGE can_message;

BOOLEAN SEN_PWR_FLAG, LC_PWR_FLAG, SEN_PWR_RETRYING, LC_PWR_RETRYING = FALSE;

BOOLEAN MCAN_ComposeMessage(MCAN_MESSAGE *message, WORD Id, BYTE Length, WORD *value);
void EIC_Initialize();

int main(void)
{
	SYSTEM_InitializeClocks(SYSTEM_OSCILLATOR_EXTOSC);
	SYSTEM_InitalizeSysTick(SYSTEM_SYSTICK_FREQ);
	
	//UART Configuration for testing
	GPIO_SetPinMode(GPIO_PORTA, 0, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinHigh(GPIO_PORTA, 0);
	GPIO_SetPinMux(GPIO_PORTA, 0, GPIO_PINMUX_SERCOMALT);
	GPIO_SetPinMode(GPIO_PORTA, 1, GPIO_PINMODE_INPUT);
	GPIO_SetPinMux(GPIO_PORTA, 1, GPIO_PINMUX_SERCOMALT);
	UART_SetMode(&UART0_PERIPH, 115200, UART_MODE_NORMAL);
	CreateStaticFifoBuffer(&UART0_RXBUFFER, _rx_fifo_mem, sizeof(_rx_fifo_mem));
	CreateStaticFifoBuffer(&UART0_TXBUFFER, _tx_fifo_mem, sizeof(_tx_fifo_mem));
	UART_LinkBuffers(&UART0_PERIPH, &UART0_RXBUFFER, &UART0_TXBUFFER);
	UART_Initialize(&UART0_PERIPH, 1);
	NVIC_EnableIRQ(SERCOM1_IRQn);
		
	//CAN Configuration
	GPIO_SetPinMode(GPIO_PORTB, 22, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinMux(GPIO_PORTB, 22, GPIO_PINMUX_CANSWD);
	GPIO_SetPinMode(GPIO_PORTB, 23, GPIO_PINMODE_INPUT);
	GPIO_SetPinMux(GPIO_PORTB, 23, GPIO_PINMUX_CANSWD);	
	MCAN_Initialize(&MCAN0);
	MCAN_ChangeMode(&MCAN0, MCAN_MODE_ENABLED);

	//ADC Configuration
	GPIO_SetPinMode(GPIO_PORTB, 8, GPIO_PINMODE_ANALOG); 	//PB8 for Vsus1
	GPIO_SetPinMux(GPIO_PORTB, 8, GPIO_PINMUX_ANALOG);
	GPIO_SetPinMode(GPIO_PORTB, 9, GPIO_PINMODE_ANALOG);	//PB9 for Vsus2
	GPIO_SetPinMux(GPIO_PORTB, 9, GPIO_PINMUX_ANALOG);
	#if(REARSENSORS)
	GPIO_SetPinMode(GPIO_PORTA, 4, GPIO_PINMODE_ANALOG);	//PA4 for PS (Pressure Sensor)
	GPIO_SetPinMux(GPIO_PORTA, 4, GPIO_PINMUX_ANALOG);
	#endif
	#if(FRONTSENSORS)
	#endif
	ADC_Initialize(ADC0, ADC_VREF_VDD);
	ADC_Initialize(ADC1, ADC_VREF_VDD);

	//Overcurrent protection configuration
	GPIO_SetPinMode(GPIO_PORTA, 12, GPIO_PINMODE_OUTPUT); 	//SEN_PWR_EN
	GPIO_SetPinLow(GPIO_PORTA, 12);
	GPIO_SetPinMode(GPIO_PORTB, 10, GPIO_PINMODE_OUTPUT);	//LC_PWR_EN
	GPIO_SetPinLow(GPIO_PORTB, 10);
	GPIO_SetPinMode(GPIO_PORTA, 13, GPIO_PINMODE_INPUT_PULLUP); 	//SEN_PWR_FAULT (EXTINT13)
	GPIO_SetPinHigh(GPIO_PORTA, 13);								//Library not pulling up
	GPIO_SetPinMux(GPIO_PORTA, 13, GPIO_PINMUX_EXTINT);
	GPIO_SetPinMode(GPIO_PORTB, 11, GPIO_PINMODE_INPUT_PULLUP);		//LC_PWR_FAULT (EXTINT11)
	GPIO_SetPinHigh(GPIO_PORTB, 11);								//Library not pulling up
	GPIO_SetPinMux(GPIO_PORTB, 11, GPIO_PINMUX_EXTINT);
	
	//GPIO Configuration for time testing
	GPIO_SetPinMode(GPIO_PORTA, 20, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinLow(GPIO_PORTA, 20);
	GPIO_SetPinMode(GPIO_PORTA, 22, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinLow(GPIO_PORTA, 22);
	
	/*Initialize overcurrent protection
		IF Sen_pwr_faults OR LC_pwr_faults ARE GREATER THEN 0 THERE IS A FAULT GOING ON, THE REGARDING SENSORS SHOULD NOT BE HANDLED
		SO WHEN CHECKING IF IT IS TIME TO SAMPLE pwr_faults == 0 MUST BE ASSERTED
	*/
	CHAR Sen_pwr_faults, LC_pwr_faults = 0;			
	DWORD Last_sen_pwr_retry, Last_LC_pwr_retry = 0;
	EIC_Initialize();
	GPIO_SetPinHigh(GPIO_PORTA, 12);	//Enable SEN_PWR
	GPIO_SetPinHigh(GPIO_PORTB, 10);	//Enable LC_PWR
	DWORD Actual = SYSTEM_GetSysTickCount();
	while(SYSTEM_GetSysTickCount() - Actual <= 20){};	//Wait for Td_fault time to check the proper working condition;
	if (GPIO_GetPinState(GPIO_PORTA, 13) == 0){			//Fault pin LOW beetween 20 and 30 ms indicates a fault
		Sen_pwr_faults++;
	}
	if (GPIO_GetPinState(GPIO_PORTB, 11) == 0){
		LC_pwr_faults++;
	}
	EIC->INTENSET.reg = EIC_INTENSET_EXTINT(EXTINT(11) | EXTINT(13)); //Enable EIC interrupt for channel 11 and 13
	NVIC_EnableIRQ(EIC_IRQn);
	
	// Temporization variables
	DWORD Last_Suspension, Last_EngineTemp;
	#if(REARSENSORS)
	DWORD Last_Pressure, Last_WaterTemp;
	Actual = Last_Suspension = Last_EngineTemp = Last_Pressure = Last_WaterTemp = SYSTEM_GetSysTickCount(); 
	#endif
	#if(FRONTSENSORS)
	DWORD Last_BrakeTemp, Last_LoadCell;
	Actual = Last_Suspension = Last_EngineTemp = Last_BrakeTemp = Last_LoadCell = SYSTEM_GetSysTickCount();
	#endif
	
	//Data variables
	WORD Suspension[2]={0,0};
	#if(REARSENSORS)
	WORD Pressure = 0;
	#endif
	#if(FRONTSENSORS)
	#endif

	//Send errors overview message
	//MCAN_ComposeMessage(&can_message, CANID_ONLINE, 0, NULL);
	//MCAN_TransmitMessageFIFO(&MCAN0, &can_message);

	while(1) {
		Actual = SYSTEM_GetSysTickCount();

		// Sensors power fault handling and retry
		if (SEN_PWR_FLAG==1){
			SEN_PWR_FLAG = 0;
			if (GPIO_GetPinState(GPIO_PORTA, 13) == 0){
				SEN_PWR_RETRYING = 0;
				if(Sen_pwr_faults == 0){
					//Send senpwrfault message
				}
				if(Sen_pwr_faults++ >= PWR_FAULT_TRIES){				//when the number of consecutive faults reach the limit disable the sensor power and interrupts
					EIC->INTENCLR.reg = EIC_INTENCLR_EXTINT(EXTINT(13));
					GPIO_SetPinLow(GPIO_PORTA, 12);
				}
			}
			if (GPIO_GetPinState(GPIO_PORTA, 13) == 1){
				SEN_PWR_RETRYING = 1;
				Last_sen_pwr_retry = Actual;
			}
			//FifoBuffer_WriteString(&UART0_TXBUFFER, "A13\n");
		}
		if(SEN_PWR_RETRYING && (Actual - Last_sen_pwr_retry > 20)){		//when the overcurrent protection is trying again for more than 20ms then the fault is gone and the faults counter brought to zero
			SEN_PWR_RETRYING = FALSE;
			Sen_pwr_faults = 0;
		}

		// Load cell power fault handling and retry
		if (LC_PWR_FLAG==1){
			LC_PWR_FLAG = 0;
			if (GPIO_GetPinState(GPIO_PORTB, 11) == 0){
				LC_PWR_RETRYING = 0;
				if(LC_pwr_faults == 0){
					//Send LCpwrfault message
				}
				if(LC_pwr_faults++ >= PWR_FAULT_TRIES){				//when the number of consecutive faults reach the limit disable the sensor power and interrupts
					EIC->INTENCLR.reg = EIC_INTENCLR_EXTINT(EXTINT(11));
					GPIO_SetPinLow(GPIO_PORTB, 10);
				}
			}
			if (GPIO_GetPinState(GPIO_PORTB, 11) == 1){
				LC_PWR_RETRYING = 1;
				Last_LC_pwr_retry = Actual;
			}
			//FifoBuffer_WriteString(&UART0_TXBUFFER, "A13\n");
		}
		if(LC_PWR_RETRYING && (Actual - Last_LC_pwr_retry > 20)){		//when the overcurrent protection is trying again for more than 20ms then the fault is gone and the faults counter brought to zero
			LC_PWR_RETRYING = FALSE;
			LC_pwr_faults = 0;
		}
		
		// Data Sampling and Handling
		if ((Sen_pwr_faults == 0) && (Actual - Last_Suspension > SUSPENSION_T - 1)){    // -1 empirical correction value (11ms measured)
			Last_Suspension = Actual;
			//GPIO_SetPinHigh(GPIO_PORTA, 20);
			ADC_SetSingleEndedChannel(ADC0, 2); //PB8 for Vsus1
			ADC_SetSingleEndedChannel(ADC1, 5); //PB9 for Vsus2
			ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
			ADC_StartConversion(ADC1, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC0));
			Suspension[0] = ADC_GetResult(ADC0);
			while(!ADC_IsResultReady(ADC1));
			Suspension[1] = ADC_GetResult(ADC1);
			MCAN_ComposeMessage(&can_message, CANID_SUSPENSION, sizeof(Suspension), Suspension);
			MCAN_TransmitMessageFIFO(&MCAN0, &can_message);
			//GPIO_SetPinLow(GPIO_PORTA, 20);
			//sprintf(strbuf, "PB8: %d PB9: %d\n", Suspension[0], Suspension[1]);			
			//FifoBuffer_WriteString(&UART0_TXBUFFER, strbuf);
		}
		if (Actual - Last_EngineTemp > ENGINETEMP_T){
			Last_EngineTemp = Actual;
			//SAMPLE and send
		}
		#if(REARSENSORS)
		if ((Sen_pwr_faults == 0) && (Actual - Last_Pressure > PRESSURE_T - 1)){		// -1 empirical correction value (201ms measured)
			Last_Pressure = Actual;
			//GPIO_SetPinHigh(GPIO_PORTA, 20);
			//GPIO_SetPinHigh(GPIO_PORTA, 22);
			ADC_SetSingleEndedChannel(ADC0, 4);	//PA4 for PS (Pressure Sensor)
			ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC0));
			Pressure = ADC_GetResult(ADC0);
			MCAN_ComposeMessage(&can_message, CANID_PRESSURE, sizeof(Pressure), &Pressure);
			MCAN_TransmitMessageFIFO(&MCAN0, &can_message);
			//GPIO_SetPinLow(GPIO_PORTA, 22);
			//GPIO_SetPinLow(GPIO_PORTA, 20);
			//sprintf(strbuf, "PA4: %d\n", Pressure);			
			//FifoBuffer_WriteString(&UART0_TXBUFFER, strbuf);
		}
		if (Actual - Last_WaterTemp > WATERTEMP_T){
			Last_WaterTemp = Actual;
			//SAMPLE and send
		}
		#endif
		#if(FRONTSENSORS)
		if (Actual - Last_BrakeTemp > BRAKETEMP_T){
			Last_BrakeTemp = Actual;
			//SAMPLE and send
		}
		if (Actual - Last_LoadCell > LOADCELL_T){
			Last_LoadCell = Actual;
			//SAMPLE and send
		}
		#endif
	}
}

/*
This Function compose the CAN message:
- message is the pointer to MCAN_MESSAGE to compose
- Id is the Id for the CAN message
- Length is the number of bytes to fit in the message (8 max.), best used with "sizeof()"
- value is the pointer to the value (requiers & in the call) or the array of values to use, must be NULL or 0 if no value has to be sent
*/
BOOLEAN MCAN_ComposeMessage(MCAN_MESSAGE *message, WORD Id, BYTE Length, WORD *value){
	if(Length > 8)
		return FALSE;
	message->Id = Id;
	message->Lenght = Length; //Lenght equal to the number of bytes
	for( BYTE i = 0; i < Length; i++){
		message->Data[i]= i%2 == 0 ?  (BYTE)(*(value+i/2) & 0xFF) : (BYTE)(*(value+i/2) >> 8);
	}
	return TRUE;
}


void EIC_Initialize(void){
	MCLK->APBAMASK.bit.EIC_ = 1;
	EIC->CONFIG[1].reg = EIC_CONFIG_SENSE3_BOTH | EIC_CONFIG_SENSE5_BOTH;	//Enable EXTINT[11] e EXTINT[13] on both edge detection
	EIC->ASYNCH.reg = EXTINT(11) | EXTINT(13);		//Interrupt Asincroni
	EIC->CTRLA.bit.ENABLE = 1;
	while (EIC->SYNCBUSY.bit.ENABLE){};
}

void EIC_Handler(void){
	if (EIC->INTFLAG.reg == EXTINT(11)){	//Interrupt on the LC_PWR
		EIC->INTFLAG.reg = EXTINT(11);
		LC_PWR_FLAG = 1;
	}
	if (EIC->INTFLAG.reg == EXTINT(13)){	//Interrupt on the SEN_PWR
		EIC->INTFLAG.reg = EXTINT(13);
		SEN_PWR_FLAG = 1;		
	}
}